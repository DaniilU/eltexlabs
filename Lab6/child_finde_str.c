#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int res;

	if (argc != 3)
	{
		printf("few args");
		return -1;
	}

	res = strcmp(argv[1], argv[2]);

	if (res == 0)
	{
		printf("Found str\n");
		return 1;
	}
	else
	{
		printf("Not found str\n");
		return 0;
	}
}