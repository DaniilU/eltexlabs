#include <stdio.h>
#include <string.h>

#define MAX_STR_LEN (1000)
#define MAX_FILE_LEN (100)

void chengPref(char *str_in, char *str_out, char *str_pref)
{
	char *tmp;

	strncpy(str_out, str_in, strlen(str_out) + 1);

	if ((tmp = strrchr(str_out, '.')) == NULL)
		memcpy(str_out + strlen(str_out) - 2, str_pref, strlen(str_pref));
	else
		memcpy(tmp, str_pref, strlen(str_pref));
}

int main(int argc, char ** argv)
{
	FILE *fp_src;
	FILE *fp_dst;
	char name_file_dst[MAX_FILE_LEN];
	char ch;
	char ch_compere;
	int res;


	if (argc < 3)
	{
		fprintf(stderr, "few args\n");
		exit(1);
	}

	ch_compere = *argv[2];

	fp_src = fopen(argv[1], "r");
	if (fp_src == NULL)
	{
		printf("fopen src err\n");
		exit(1);
	}

	chengPref(argv[1], name_file_dst, ".dst");
	fp_dst = fopen(name_file_dst, "w");
	if (fp_dst == NULL)
	{
		printf("fopen dst err\n");
		fclose(fp_src);
		exit(1);
	}

	while (1)
	{
		ch = fgetc(fp_src);
		if (ch == EOF)
		{
			if (feof(fp_src) != 0)
				break;
			else
			{
				printf("fgetc err\n");
				fclose(fp_src);
				fclose(fp_dst);
				exit(1);
			}
		}
		
		if(ch != ch_compere)
		{
			res = fputc(ch,fp_dst);
			if (res == EOF)
			{
				printf("fputc err\n");
				fclose(fp_src);
				fclose(fp_dst);
				exit(1);		
			}
		}
	}
}