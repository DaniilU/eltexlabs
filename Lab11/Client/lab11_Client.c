#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>


#define MAX_STR_LEN 1000


int send_str_count_quiest(int sock)
{
	char buf = 'N';
	char rcvBuf[MAX_STR_LEN];
	send(sock, &buf, 1, 0);

	recv(sock, rcvBuf, MAX_STR_LEN, 0);

	printf("str count = %d\n", atoi(rcvBuf));
	return atoi(rcvBuf);
}

void main (int argc, char * argv[])
{
	int count = 0;
	int str_compere;
	int sock;
	struct sockaddr_in ServerAddr;
	unsigned short ServPort;
	char *ServIP;
	char rcvBuf[MAX_STR_LEN];
	char sendStr[MAX_STR_LEN];
	char *strCmp;

	printf("start client\n");
	if (argc != 4)
	{
		printf("arg err\n");
		exit(1);
	}

	ServIP = argv[1];
	ServPort = atoi(argv[2]);
	strCmp = argv[3];

	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		printf("sock err\n");
		exit(1);
	}

	memset(&ServerAddr, 0, sizeof(ServerAddr));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_addr.s_addr = inet_addr(ServIP);
	ServerAddr.sin_port = htons(ServPort);

	if (connect(sock, (struct sockaddr *)&ServerAddr, sizeof(ServerAddr)) < 0)
	{
		printf("connect err\n");
		close(sock);
		exit(1);
	}

	count = send_str_count_quiest(sock);

	for (int i = 0; i < count; i++)
	{
		sprintf(sendStr, "%d", i);
		send(sock, sendStr, strlen(sendStr), 0);
		recv(sock, rcvBuf, MAX_STR_LEN, 0);

		printf("recv str: %s\n", rcvBuf);
		str_compere = strcmp(rcvBuf, strCmp);
		if (str_compere == 0)
		{
			printf("found same str num %d\n", i + 1);
		}
		memset(rcvBuf, 0, strlen(rcvBuf) + 1);
	}

	close(sock);
}