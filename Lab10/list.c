#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

str_list_t * list_init (void)
{
	str_list_t * list = calloc(1, sizeof(str_list_t));
	if (!list)
	{
		printf("calloc err\n");
		return NULL;
	}

	return list;
}

str_list_t * list_node_create (char * str)
{
	if (str == NULL)
	{
		printf("err arg\n");
		return NULL;
	}

	str_list_t *n = calloc(1, sizeof(str_list_t));
	if (!n)
	{
		printf("calloc err\n");
		return NULL;
	}

	n->str = calloc(strlen(str) + 1, sizeof(char));
	if (!n->str)
	{
		printf("calloc err\n");
		free(n);
		return NULL;
	}

	strncpy(n->str, str, strlen(n->str) + 1);

	return n;
}

void list_add_tail (str_list_t *list, str_list_t *node)
{
	str_list_t *current = list;

	while(current->next != NULL)
		current = current->next;

	current->next = node;
}

void free_node (str_list_t *node)
{
	free(node->str);
	node->str = NULL;
	free(node);
	node = NULL;

}

int add_str_tail_list (str_list_t *list, char * str)
{
	str_list_t *n = list_node_create(str);
	if (n == NULL)
	{
		printf("list_node_create err\n");
		return -1;
	}

	list_add_tail(list, n);

	return 0;
}

void free_list (str_list_t *list)
{
	str_list_t *current = list;
	str_list_t *tmp;

	free(list->str);
	tmp = current->next;

	while(current->next != NULL)
	{
		current = tmp->next;
		tmp = current->next;

		free(current->str);
		free(current);

	}
}

void target_list (str_list_t *list, str_list_t **target_node, int target)
{
	if(target == 0)
	{
		*target_node = list;
		return;
	}

	str_list_t *current = list;
	int tmp = 0;

	while(current->next != NULL)
	{
		if(tmp == target - 1)
			break;
		current = current->next;
		tmp++;
	}
	*target_node =  current->next;
}

void print_list (str_list_t *list)
{
	str_list_t *current = list;

	while(current->next != NULL)
	{
		printf("Str: %s\n", current->str);
		current = current->next;
	}
}