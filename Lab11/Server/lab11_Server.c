#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include "list.h"

#define RCVBUFSIZE (10)

#define MAX_STR_LEN (1000)
#define MAXPENDING (5)

extern int errno;

int count_str = 0;

int write_file_str_list (FILE * fd, str_list_t *list)
{
	int count_str = 0;
	char tmp_str[MAX_STR_LEN];
	char *res;
	char *adr_n;

	res = fgets(tmp_str, MAX_STR_LEN, fd);
	if (res == NULL)
	{
		if (feof(fd) != 0)
		{
			printf("File is empty\n");
			return -1;
		}
		else
		{
			printf("fgets err\n");
			return -1;
		}
	}
	count_str++;


	if ((adr_n = strchr(tmp_str, '\n')) != NULL)
		*adr_n = '\0'; 
	add_str_tail_list(list, tmp_str);

	while (1)
	{
		res = fgets(tmp_str, MAX_STR_LEN, fd);
		if (res == NULL)
		{
			if (feof(fd) != 0)
				break;
			else
			{
				printf("fgets err\n");
				return -1;
			}
		}
		count_str++;
		if ((adr_n = strchr(tmp_str, '\n')) != NULL)
			*adr_n = '\0'; 
		add_str_tail_list(list, tmp_str);
	}

	return count_str;
}

void send_num_str_to_sock(int sock)
{
	char buf[RCVBUFSIZE];
	sprintf(buf, "%d", count_str);
	send(sock, buf, strlen(buf), 0);
}

void send_str_list_to_sock(int sock, str_list_t *node, int num)
{
	str_list_t *tmp_str_listp = node;

	target_list(node, &tmp_str_listp, num);
	send(sock, tmp_str_listp->str, strlen(tmp_str_listp->str), 0);
}

void send_err_to_sock(int sock)
{
	char buf[] = "err mes";
	send(sock, buf, strlen(buf), 0);
}

void main (int argc, char * argv[])
{
	str_list_t *str_list1 = list_init();
	if (str_list1 == NULL)
	{
		printf("list_init err\n");
		exit(1);
	}

	int servSock;
	int clntSock;
	struct sockaddr_in ServerAddr;
	struct sockaddr_in ClntAddr;
	unsigned short ServPort;
	char *ServIP;
	char Buf[RCVBUFSIZE];

	ServPort = atoi(argv[1]);

	FILE * fd = fopen(argv[2], "r");
	if (fd == NULL)
	{
		perror("fopen err\n");
		free_list(str_list1);
		exit(1);
	}

	printf("start\n");
	count_str = write_file_str_list(fd, str_list1);
	if (count_str == -1)
	{
		printf("write_file_str_list err\n");
		fclose(fd);
		free_list(str_list1);
		exit(1);
	}

	print_list(str_list1);

	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		printf("sock err\n");
		free_list(str_list1);
		fclose(fd);
		exit(1);
	}

	memset(&ServerAddr, 0, sizeof(ServerAddr));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	ServerAddr.sin_port = htons(ServPort);

	if (bind(servSock, (struct socaddr *) &ServerAddr, sizeof(ServerAddr)) < 0)
	{
		printf("bind err\n");
		free_list(str_list1);
		fclose(fd);
		close(servSock);
		exit(1);
	}

	if (listen(servSock, MAXPENDING) < 0)
	{
		printf("listen err\n");
		free_list(str_list1);
		fclose(fd);
		close(servSock);
		exit(1);
	}

	clntSock = accept(servSock, (struct sockaddr *)&ClntAddr, &clntLen);
	if (clntSock < 0)
	{
		printf("accept err\n");
		free_list(str_list1);
		fclose(fd);
		close(servSock);
		exit(1);
	}


	while(1)
	{

		if (recv(clntSock, Buf, MAXPENDING, 0) < 0)
		{
			if (errno == EPIPE)
				break;
			else
			{
				printf("recv err\n");
				free_list(str_list1);
				fclose(fd);
				close(servSock);
				exit(1);
			}
		}

		printf("(Serv) recv str: %s\n", Buf);

		if (Buf[0] == 'N')
			send_num_str_to_sock(clntSock);
		else if (atoi(Buf) < count_str && atoi(Buf) >= 0)
			send_str_list_to_sock(clntSock, str_list1, atoi(Buf));
		else
			send_err_to_sock(clntSock);

	}

	close(servSock);
	free_list(str_list1);
	fclose(fd);

}