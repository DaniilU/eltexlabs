#include <stdio.h>
#include <string.h>

#define MAX_STR_LEN (1000)
#define MAX_FILE_LEN (100)

void chengPref(char *str_in, char *str_out, char *str_pref)
{
	char *tmp;

	strncpy(str_out, str_in, strlen(str_out) + 1);

	if ((tmp = strrchr(str_out, '.')) == NULL)
		memcpy(str_out + strlen(str_out) - 2, str_pref, strlen(str_pref));
	else
		memcpy(tmp, str_pref, strlen(str_pref));
}

int main(int argc, char ** argv)
{
	FILE *fp_src;
	FILE *fp_dst;
	int str_len_thr = 0;

	char str[MAX_STR_LEN];
	char name_file_dst[MAX_FILE_LEN];
	int res;


	if (argc < 3)
	{
		fprintf(stderr, "few args\n");
		exit(1);
	}

	str_len_thr = atoi(argv[2]);
	if (str_len_thr == 0)
	{
		printf("argv[2] err or argv[2] = 0\n");
		exit(1);
	}

	fp_src = fopen(argv[1], "r");
	if (fp_src == NULL)
	{
		printf("fopen src err\n");
		exit(1);
	}

	chengPref(argv[1], name_file_dst, ".dst");
	fp_dst = fopen(name_file_dst, "w");
	if (fp_dst == NULL)
	{
		printf("fopen dst err\n");
		fclose(fp_src);
		exit(1);
	}

	while (1)
	{
		res = fgets(str, MAX_STR_LEN, fp_src);
		if (res == NULL)
		{
			if (feof(fp_src) != 0)
				break;
			else
			{
				printf("fgets err\n");
				fclose(fp_src);
				fclose(fp_dst);
				exit(1);
			}
		}
		if (strlen(str) <= str_len_thr + 1)
		{
			res = fputs(str, fp_dst);
			if (res == EOF)
			{
				printf("fputs err\n");
				fclose(fp_src);
				fclose(fp_dst);
				exit(1);		
			}
		}
	}
}