#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include "list.h"

#define MAX_STR_LEN (1000)

str_list_t str_list1;
pthread_mutex_t mutex;

int flag_eof = 0;


int write_str_end_list(char *str)
{
	str_list_t *current = &str_list1;

	while(current->next != NULL)
	{
		current = current->next;
	}
	pthread_mutex_lock(&mutex);

	current->next = (str_list_t *)calloc(1, sizeof(str_list_t));
	if (!current->next)
	{
		printf("next calloc err\n");
		return -1;
	}

	current->str = (char *)calloc(strlen(str) + 1, sizeof(char));
	if (!current->str)
	{
		printf("str calloc err\n");
		return -1;
	}

	strncpy(current->str, str, strlen(current->str) + 1);
	current->next->next = NULL;

	pthread_mutex_unlock(&mutex);

	return 0;
}

int first_write_file_str_list(FILE *fd)
{
	char tmp_str[MAX_STR_LEN];
	char *res;
	char *adr_n;

	res = fgets(tmp_str, MAX_STR_LEN, fd);
	if (res == NULL)
	{
		if (feof(fd) != 0)
		{
			printf("File is empty\n");
			return -1;
		}
		else
		{
			printf("fgets err\n");
			return -1;
		}
	}

	if ((adr_n = strchr(tmp_str, '\n')) != NULL)
		*adr_n = '\0'; 
	if (-1 == write_str_end_list(tmp_str))
	{
		printf("write_str_end_list err\n");
		return -1;
	}

	return 0;
}

void * write_file_str_list_thread(void *arg)
{
	FILE *fd = (FILE *)arg;

	char tmp_str[MAX_STR_LEN];
	char *res;
	char *adr_n;

	while (1)
	{
		res = fgets(tmp_str, MAX_STR_LEN, fd);
		if (res == NULL)
		{
			if (feof(fd) != 0)
			{
				flag_eof = 1;
				printf("EOF\n");
				print_list(&str_list1);
				break;
			}
		
			else
			{
				pthread_mutex_lock(&mutex);
				printf("fgets err\n");
				free_list(&str_list1);
				fclose(fd);
				pthread_mutex_unlock(&mutex);
				exit(1);
			}
		}
		if ((adr_n = strchr(tmp_str, '\n')) != NULL)
			*adr_n = '\0'; 
		if (-1 == write_str_end_list(tmp_str))
		{
			printf("write_str_end_list err\n");
			free_list(&str_list1);
			fclose(fd);
			exit(1);
		}
	}
	pthread_exit(NULL);
}

void * read_list_thread(void *arg)
{
	char *str2 = (char *)arg;
	str_list_t *current = &str_list1;
	int str_compare;
	int count = 0;

	while(1)
	{
		pthread_mutex_lock(&mutex);
		if (current->next != NULL)
		{
			count++;
			str_compare = strcmp(current->str, str2);
			if (str_compare == 0)
			{
				printf("found same str num %d\n", count);
			}
			current = current->next;
		}
		else
		{
			usleep(10000);

			if (flag_eof == 1)
			{
				pthread_mutex_unlock(&mutex);
				break;
			}
		}
		pthread_mutex_unlock(&mutex);
	}
	pthread_exit(NULL);
}

void main (int argc, char * argv[])
{
	str_list1.next = NULL;

	pthread_t tid_write_list, tid_read_list;
	int res;

	pthread_mutex_init(&mutex, NULL);

	FILE * fd = fopen(argv[1], "r");
	if (fd == NULL)
	{
		perror("fopen err\n");
		free_list(&str_list1);
		exit(1);
	}

	struct stat buff;
	stat(argv[1], &buff);
	if (buff.st_size == 0)
	{
		printf("File is empty\n");
		free_list(&str_list1);
		exit(1);
	}

	res = pthread_create(&tid_write_list, NULL, write_file_str_list_thread, (void *)fd);
	if (res != 0)
	{
		printf("tid_write_list pthread_create err\n");
		free_list(&str_list1);
		fclose(fd);
		exit(1);
	}

	res = pthread_create(&tid_read_list, NULL, read_list_thread, (void *)argv[2]);
	if (res != 0)
	{
		printf("tid_read_list pthread_create err\n");
		free_list(&str_list1);
		fclose(fd);
		exit(1);
	}

	pthread_join(tid_write_list, NULL);
	pthread_join(tid_read_list, NULL);

	printf("end\n");

	free_list(&str_list1);
	fclose(fd);
}