#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "list.h"

#define MAX_STR_LEN (1000)

struct mymsbuf
{
	long mtype;
	char mtext[MAX_STR_LEN];
};

int write_file_str_list (FILE * fd, str_list_t *list)
{
	int count_str = 0;
	char tmp_str[MAX_STR_LEN];
	char *res;
	char *adr_n;

	res = fgets(tmp_str, MAX_STR_LEN, fd);
	if (res == NULL)
	{
		if (feof(fd) != 0)
		{
			printf("File is empty\n");
			return -1;
		}
		else
		{
			printf("fgets err\n");
			return -1;
		}
	}
	count_str++;


	if ((adr_n = strchr(tmp_str, '\n')) != NULL)
		*adr_n = '\0'; 
	add_str_tail_list(list, tmp_str);

	while (1)
	{
		res = fgets(tmp_str, MAX_STR_LEN, fd);
		if (res == NULL)
		{
			if (feof(fd) != 0)
				break;
			else
			{
				printf("fgets err\n");
				return -1;
			}
		}
		count_str++;
		if ((adr_n = strchr(tmp_str, '\n')) != NULL)
			*adr_n = '\0'; 
		add_str_tail_list(list, tmp_str);
	}

	return count_str;
}

int send_mes(int qid, struct mymsbuf *qbuf, long type, char *text)
{
	int tmp;

	qbuf->mtype = type;
	strncpy(qbuf->mtext, text, strlen(qbuf->mtext) + 1);

	tmp = msgsnd(qid, (struct msqbuf *)qbuf, strlen(qbuf->mtext) + 1, 0);
	if (tmp == -1)
	{
		printf("msgsnd err\n");
		return -1;
	}

	return 0;
}

int read_mes(int qid, struct mymsbuf *qbuf, long type)
{
	qbuf->mtype = type;
	if (-1 == msgrcv(qid, (struct msgbuf *)qbuf, MAX_STR_LEN, type, 0))
	{
		printf("msgrcv err\n");
		return -1;
	}

	return 0;
}

void main (int argc, char * argv[])
{
	int count_str = 0;
	int resalt_count = 0;
	int status, stat;
	int resalt = 0;
	int str_compere;
	key_t key;
	int qtype = 1;
	struct mymsbuf qbuf;
	int msqid;
	char tmp_str[2];

	str_list_t *str_list1 = list_init();
	if (str_list1 == NULL)
	{
		printf("list_init err\n");
		exit(1);
	}
	str_list_t *tmp_str_listp;


	key = ftok(".", 'm');
	msqid = msgget(key, IPC_CREAT|0660);
	if (msqid == -1)
	{
		printf("msgget err\n");
		free_list(str_list1);
		exit(1);
	}

	FILE * fd = fopen(argv[1], "r");
	if (fd == NULL)
	{
		perror("fopen err\n");
		free_list(str_list1);
		msgctl(msqid, IPC_RMID, NULL);
		exit(1);
	}

	count_str = write_file_str_list(fd, str_list1);
	if (count_str == -1)
	{
		printf("write_file_str_list err\n");
		fclose(fd);
		free_list(str_list1);
		msgctl(msqid, IPC_RMID, NULL);
		exit(1);
	}

	pid_t pid[count_str];

	for(int i = 0; i < count_str; i++)
	{
		target_list(str_list1, &tmp_str_listp, i);
		
		pid[i] = fork();
		if (pid[i] == -1)
		{
			perror("fork");
			fclose(fd);
			free_list(str_list1);
			msgctl(msqid, IPC_RMID, NULL);
			exit(1);
		}
		else if (pid[i] == 0)
		{

			str_compere = strcmp(tmp_str_listp->str, argv[2]);
			if (str_compere == 0)
				sprintf(tmp_str, "%d", str_compere);
			else
				sprintf(tmp_str, "%d", 1);
			if (-1 = send_mes(msqid, &qbuf, qtype, tmp_str))
			{
				printf("send_mes err\n");
				exit(-1);
			}
			exit(1);
		}

	}
	for(int i = 0; i < count_str; i++)
	{
		status = waitpid(pid[i], &stat, 0);
		if (status == -1)
		{
			printf("waitpid err\n");
		}
		if (WIFEXITED(stat)) 
		{
			if (WEXITSTATUS(stat) == -1)
			{
				printf("pid err\n");
				fclose(fd);
				free_list(str_list1);
				msgctl(msqid, IPC_RMID, NULL);
				exit(1);
			}
			else
			{
				if (-1 == read_mes(msqid, &qbuf, qtype))
				{
					printf("read_mes err\n");
					fclose(fd);
					free_list(str_list1);
					msgctl(msqid, IPC_RMID, NULL);
					exit(1);
				}
				resalt = atoi(qbuf.mtext);
				if(resalt == 0)
				{
					resalt_count++;
					printf("str num %d is same\n", i + 1);
				}	
			}
		}
	}
	printf("resalt str count = %d\n", resalt_count);

	free_list(str_list1);
	msgctl(msqid, IPC_RMID, NULL);
	fclose(fd);
}