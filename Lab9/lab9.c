#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include "list.h"


#define MAX_STR_LEN (1000)

union semun
{
	int val;
	struct semid_ds *buf;
	unsigned short *array;

	struct seminfo *__buf;
};

int write_file_str_list (FILE * fd, str_list_t *list)
{
	int count_str = 0;
	char tmp_str[MAX_STR_LEN];
	char *res;
	char *adr_n;

	res = fgets(tmp_str, MAX_STR_LEN, fd);
	if (res == NULL)
	{
		if (feof(fd) != 0)
		{
			printf("File is empty\n");
			return -1;
		}
		else
		{
			printf("fgets err\n");
			return -1;
		}
	}
	count_str++;


	if ((adr_n = strchr(tmp_str, '\n')) != NULL)
		*adr_n = '\0'; 
	add_str_tail_list(list, tmp_str);

	while (1)
	{
		res = fgets(tmp_str, MAX_STR_LEN, fd);
		if (res == NULL)
		{
			if (feof(fd) != 0)
				break;
			else
			{
				printf("fgets err\n");
				return -1;
			}
		}
		count_str++;
		if ((adr_n = strchr(tmp_str, '\n')) != NULL)
			*adr_n = '\0'; 
		add_str_tail_list(list, tmp_str);
	}

	return count_str;
}

int free_and_exit( int rc)
{
	if (fd)
	{
		fclose(fd);
		fd = NULL;
	}

	if (str_list1)
	{
		free_list(str_list1);
		str_list1 = NULL;
	}

	if (semid)
	{
		semctl(semid, 0, IPC_RMID);
		semid = 0;
	}

	if (shmid)
	{
		shmctl(shmid, IPC_RMID, 0);
		shmid = 0;
	} 

	return rc;
}

int main (int argc, char * argv[])
{
	int count_str = 0;
	int resalt_count = 0;
	int status, stat;
	int resalt = 0;
	int str_compere;

	str_list_t *str_list1 = list_init();
	if (str_list1 == NULL)
	{
		printf("list_init err\n");
		exit(1);
	}
	str_list_t *tmp_str_listp;

	int semid = 0;
	union semun arg;
	struct sembuf lock_res = {0, -1, 0};
	struct sembuf rel_res = {0, 1, 0};

	int shmid = 0;
	key_t key = 0;
	int *shm; 

	key = ftok(".", 'S');
	if (key < 0)
	{
		printf("ftok err\n");
		free_and_exit(-1);
	}

	semid = semget(key, 1, 0666 | IPC_CREAT);
	if (semid == -1)
	{
		printf("semget err\n");
		free_and_exit(-1);
	}

	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);

	shmid = shmget(key, sizeof(int), IPC_CREAT | 0666);
	if (shmid < 0)
	{
		printf("shmget err\n");
		free_and_exit(-1);
	}

	FILE * fd = fopen(argv[1], "r");
	if (fd == NULL)
	{
		perror("fopen err\n");
		free_and_exit(-1);
	}

	count_str = write_file_str_list(fd, str_list1);
	if (count_str == -1)
	{
		printf("write_file_str_list err\n");
		free_and_exit(-1);
	}

	pid_t pid[count_str];

	for(int i = 0; i < count_str; i++)
	{
		target_list(str_list1, &tmp_str_listp, i);
		pid[i] = fork();

		if (pid[i] == -1)
		{
			perror("fork");
			free_and_exit(-1);
		}
		else if (pid[i] == 0)
		{
			if (strcmp(tmp_str_listp->str, argv[2]) == 0)
				str_compere = 1;
			else
				str_compere = 0;

			shm = (int *)shmat(shmid, NULL, 0);
			if (shm == (int *)-1)
			{
				printf("shmat err\n");
				exit(-1);
			}

			semop(semid, &lock_res, 1);

			*(shm) = *(shm) + str_compere;

			semop(semid, &rel_res, 1);

			if (-1 == shmdt(shm))
			{
				printf("shmdt err\n");
				exit(-1);
			}

			if (str_compere == 1)
				exit(i + 1);
			else
				exit(0);
		}
	}
	for(int i = 0; i < count_str; i++)
	{
		status = waitpid(pid[i], &stat, 0);
		if (status == -1)
		{
			printf("waitpid err\n");
			free_and_exit(-1);
		}
		if (WIFEXITED(stat))
		{
			resalt = WEXITSTATUS(stat);
			if (resalt == -1)
			{
				printf("pid err\n");
				free_and_exit(-1);
			}
			if (resalt)
				printf("str num %d is same\n", resalt);
		}
	}

	shm = (int *)shmat(shmid, NULL, 0);
	if (shm == (int *)-1)
	{
		printf("shmat err\n");
		free_and_exit(-1);
	}

	printf("resalt str count = %d\n", *(shm));

	free_and_exit(1);
}