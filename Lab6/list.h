#ifndef LIST_H_
#define LIST_H_

typedef struct str_list
{
	struct str_list *next;
	char *str;
} str_list_t;

str_list_t * list_init (void);
str_list_t * list_node_create (char * str);
void list_add_tail (str_list_t *list, str_list_t *node);
void free_node (str_list_t *node);
int add_str_tail_list (str_list_t *list, char * str);
void free_list(str_list_t *list);
void target_list(str_list_t *list, str_list_t **target_node, int target);
void print_list(str_list_t *list);

#endif