#include <stdio.h>
#include <stdlib.h>
#include "arithmetic.h"
#include <dlfcn.h>

void main (void)
{
	void *lib_handl;
	int a = 10;
	int b = 3;

	lib_handl = dlopen("/home/daniil/Eltex/libarithmetic.so", RTLD_LAZY);
	if (!lib_handl)
	{
		printf("lid err");
		exit(1);
	}
	dlsym(lib_handl, "add");
	dlsym(lib_handl, "sub");
	dlsym(lib_handl, "mult");
	printf("a + b = %d\n", add(a,b));
	printf("a - b = %d\n", sub(a,b));
	printf("a * b = %d\n", mult(a,b));

	dlclose(lib_handl);
}