#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MES_LEN (50)
#define NUM_WORKERS (3)

typedef struct
{
	char worker_name[MES_LEN];
	int worker_age;
	int department_num;
	int salary;
} employee_t;

int readEmployee(employee_t *st)
{
	char tmp_worker_name[MES_LEN];
	int tmp_worker_age;
	int tmp_department_num;
	int tmp_salary;

	int tmp_res;

	printf("worker_name?\n");
	tmp_res = scanf("%49s", tmp_worker_name);
	if (tmp_res != 1)
	{
		printf("scanf err\n");
		return -1;
	}
	printf("worker_age?\n");
	tmp_res = scanf("%d", &tmp_worker_age);
	if (tmp_res != 1)
	{
		printf("scanf err\n");
		return -1;
	}
	printf("dep?\n");
	tmp_res = scanf("%d", &tmp_department_num);
	if (tmp_res != 1)
	{
		printf("scanf err\n");
		return -1;
	}
	printf("salary?\n");
	tmp_res = scanf("%d", &tmp_salary);
	if (tmp_res != 1)
	{
		printf("scanf err\n");
		return -1;
	}
	printf("\n");

	st->worker_age = tmp_worker_age;
	st->department_num = tmp_department_num;
	st->salary = tmp_salary;
	strncpy(st->worker_name, tmp_worker_name, sizeof(st->worker_name));
	return 0;
}

static int cmp(const void *l, const void *r)
{
	return (*(employee_t*)r).worker_age - (*(employee_t*)l).worker_age;
}

void print_employee_t(employee_t *st)
{
	printf("worker_name: %s\n", st->worker_name);
	printf("worker_age: %d\n", st->worker_age);
	printf("Num Otdel: %d\n", st->department_num);
	printf("salary: %d", st->salary);
	printf("\n");
}

int main(void)
{
	employee_t *buf_man = (employee_t*)calloc(NUM_WORKERS, sizeof(employee_t));
	if (buf_man == NULL)
	{
		printf("calloc err\n");
		exit(1);
	}

	for(int i = 0; i < NUM_WORKERS; i++)
	{
		if (readEmployee(&buf_man[i]) == -1)
		{
			free(buf_man);
			printf("Err readEmployee\n");
			exit(-1);
		}
	}

	qsort(buf_man, NUM_WORKERS, sizeof(employee_t *), cmp);
	
	for(int i = 0; i < NUM_WORKERS; i++)
		print_employee_t(&buf_man[i]);
	free(buf_man);
	return 0;
}