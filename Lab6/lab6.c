#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "list.h"


#define MAX_STR_LEN 1000

int write_file_str_list (FILE * fd, str_list_t *list)
{
	int count_str = 0;
	char tmp_str[MAX_STR_LEN];
	char *res;
	char *adr_n;

	while (1)
	{
		res = fgets(tmp_str, MAX_STR_LEN, fd);
		if (res == NULL)
		{
			if (feof(fd) != 0)
				break;
			else
			{
				printf("fgets err\n");
				return -1;
			}
		}
		count_str++;
		if ((adr_n = strchr(tmp_str, '\n')) != NULL)
			*adr_n = '\0'; 
		add_str_tail_list(list, tmp_str);
	}

	return count_str;
}

void main (int argc, char * argv[])
{
	int count_str = 0;
	int resalt_count = 0;
	int status, stat;
	int resalt = 0;

	str_list_t *str_list1 = list_init();
	if (str_list1 == NULL)
	{
		printf("list_init err\n");
		exit(1);
	}
	str_list_t *tmp_str_listp;

	FILE * fd = fopen(argv[1], "r");
	if (fd == NULL)
	{
		perror("fopen err\n");
		free_list(str_list1);
		exit(1);
	}

	struct stat buff;
	stat(argv[1], &buff);
	if (buff.st_size == 0)
	{
		printf("File is empty\n");
		free_list(&str_list1);
		exit(1);
	}

	count_str = write_file_str_list(fd, str_list1);
	if (count_str == -1)
	{
		printf("write_file_str_list err\n");
		fclose(fd);
		free_list(str_list1);
		exit(1);
	}

	pid_t pid[count_str];
	for(int i = 0; i < count_str; i++)
	{
		target_list(str_list1, &tmp_str_listp, i);
		pid[i] = fork();

		if (pid[i] == -1)
		{
			perror("fork");
			fclose(fd);
			free_list(str_list1);
			exit(1);
		}
		else if (pid[i] == 0)
		{
			execl("./child_finde_str", " ", tmp_str_listp->str, argv[2], NULL);
		}
	}
	for(int i = 0; i < count_str; i++)
	{
		status = waitpid(pid[i], &stat, 0);
		if (status == -1)
		{
			printf("waitpid err\n");
			fclose(fd);
			free_list(str_list1);
			exit(1);
		}
		if (WIFEXITED(stat))
		{
			resalt = WEXITSTATUS(stat);
			switch(resalt)
			{
				case -1: 
				printf("err resalt");
				break;

				case 0:
				break;

				case 1:
				printf("str number %d is same\n", i + 1);
				resalt_count++;
				break;
			}
		}
	}
	printf("resalt str count = %d\n", resalt_count);

	free_list(str_list1);
	fclose(fd);
}